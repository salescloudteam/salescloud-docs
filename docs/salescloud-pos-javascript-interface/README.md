# SalesCloud POS Javascript Interface

## Introduction

The SalesCloud POS Javascript Interface enables a two way communication between SalesCloud POS Fragments defined in SalesCloud Apps and the android SalesCloud POS app.

In short and plain terms, this allows an embedded remote website to communicate with the SalesCloud POS android app.

Use cases are but not limited to:
* Creating a new interactive UI.
* Trigger events in the app.

## The Interface

An embedded html site may access the interface by checking if "SalesCloud" is defined in javascript.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
}

```

## Methods

### startNewOrder()

This method instructs SalesCloud POS to start a new order.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.startNewOrder()
}

```

### cancelCurrentOrder()

This method instructs SalesCloud POS to cancel the current order.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.cancelCurrentOrder()
}

```

### startListeningToPayments()
   
This method instructs SalesCloud POS that your fragment wants to listen to payments.

Your web application can start to listen to payments like so.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.startListeningToPayments()
}

document.addEventListener('SalesCloudPaymentTransactionEvent', callback, false);


```

### stopListeningToPayments()
   
This method instructs SalesCloud POS that your fragment wants to stop listening to payments.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.stopListeningToPayments()
}

```

### startListeningToOrderStarted()
   
This method instructs SalesCloud POS that your fragment wants to listen to order started events.

Your web application can start to listen to order started events like so.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.startListeningToOrderStarted()
}

document.addEventListener('SalesCloudOrderStartedEvent', callback, false);


```

### stopListeningToOrderStarted()
   
This method instructs SalesCloud POS that your fragment wants to stop listening to order started events.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.stopListeningToOrderStarted()
}


```

### startListeningToOrderCompleted()
   
This method instructs SalesCloud POS that your fragment wants to listen to order completed events.

Your web application can start to listen to order completed events like so.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.startListeningToOrderCompleted()
}

document.addEventListener('SalesCloudOrderCompletedEvent', callback, false);


```

### stopListeningToOrderCompleted()
   
This method instructs SalesCloud POS that your fragment wants to stop listening to order completed events.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.stopListeningToOrderCompleted()
}


```

### addNewLineItem()
   
This method instructs SalesCloud POS to add a new line item to the current order. This method will return false if for example the product
uuid cannot be found or is not available.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   var boolean = SalesCloud.addNewLineItem(String productUUID, String quantity, String amount, String currencyCode, String lineItemLabel, String comment, String licensePlate)
}

```

### getCurrentOrderUUID()

This method instructs SalesCloud POS to return the current order uuid.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   var orderUUID = SalesCloud.getCurrentOrderUUID()
}

```

### pay(String paymentMethod, boolean showResult)
   
This method instructs SalesCloud POS to initiate a payment.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.pay()
}

```

### focus()

This method instructs SalesCloud POS to focus your app.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.startNewOrder()
}
```

### unlock()

This method instructs SalesCloud POS to unlock swiping.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.unlock()
}
```

### lock()

This method instructs SalesCloud POS to lock swiping.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.lock()
}
```

### lockRefresh()

This method instructs SalesCloud POS to lock refreshing your app.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.lockRefresh()
}
```

### unlockRefresh()

This method instructs SalesCloud POS to unlock refreshing your app.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.unlockRefresh()
}
```

### refresh()

This method instructs SalesCloud POS to refresh your app.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.refresh()
}
```

### requestOrderFragment()

This method instructs SalesCloud POS to display an orderFragment next to your app.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.requestOrderFragment()
}
```

### printHtml()

This method instructs SalesCloud POS to print your html to connected printers.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.printHtml(String html)
}
```

### startListeningToOrderCanceled()

This method instructs SalesCloud POS that your fragment wants to listen to order canceled events.

Your web application can start to listen to order canceled events like so.


``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.startListeningToOrderCanceled()
}

document.addEventListener('SalesCloudOrderCanceledEvent', callback, false);


```

### stopListeningToOrderCanceled()

This method instructs SalesCloud POS that your fragment wants to stop listening to order canceled events.

Your web application can stop to listening to order canceled events like so.


``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
   // Use your SalesCloud javascript bindings here
   SalesCloud.stopListeningToOrderCanceled()
}

document.removeEventListener('SalesCloudOrderCanceledEvent', callback);


```


### applyDeviceSettings()

This method instructs SalesCloud POS to apply device settings.

``` js

if(typeof SalesCloud !== "undefined" && SalesCloud !== null) {
    // Use your SalesCloud javascript bindings here
    SalesCloud.applyDeviceSettings(String booleanSettingsJSON, String stringSettingsJSON)
}
```