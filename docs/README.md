---
home: true
heroImage: /hero.png
actionText: Get Started →
actionLink: /salescloud-js/
footer: Copyright © 2012 - present SalesCloud
---

<div style="text-align: center">
  <Bit/>
</div>

<div class="features">
  <div class="feature">
    <h2><a href="/salescloud-apps">SalesCloud Apps</a></h2>
    <p>Create native internal apps to extend the SalesCloud Platform that merchants can discover & install.</p>
  </div>
  <div class="feature">
    <h2><a href="/salescloud-graph">SalesCloud Graph</a></h2>
    <p>Create external apps to query and mutate data with the SalesCloud Platform through a graph api.</p>
  </div>
  <div class="feature">
    <h2><a href="https://www.salescloud.is/developers/salescloud-js">SalesCloud JS</a></h2>
    <p>Write html elements that become alive in any external websites with a markup driven approach.</p>
  </div>
  <div class="feature">
      <h2><a href="/salescloud-payment-request-api">Payment Request API</a></h2>
      <p>Connect to a simple yet powerful api that can process up to 100k payments per second.</p>
  </div>
  <div class="feature">
      <h2><a href="https://support.salescloud.is/en/articles/3947511-how-to-use-the-button-builder">Button Builder</a></h2>
      <p>Learn how to use our button builder to generate salescloud js add to cart buttons.</p>
  </div>
  <div class="feature">
        <h2><a href="/webhooks">Webhooks</a></h2>
        <p>Learn how to trigger and process webhooks when needed.</p>
  </div>
  <div class="feature">
          <h2><a href="/salescloud-pos-javascript-interface">SalesCloud POS Javascript Interface</a></h2>
          <p>A javascript interface that integrates with SalesCloud POS so that web apps can send instructions.</p>
    </div>
</div>
