# Mappings

## Introduction

Imagine you pull and import product data from a remote accounting system into the SalesCloud Platform. Remembering which 
product in salescloud is which product from the external system can be a tedious step in development.

A mapping represents a bi-directional relationship between internal objects in the SalesCloud Platform and external objects in third-party systems.

Use mappings to save these relationships. The mapping framework will reward you will simpler and more maintainable code.