# Locations

## Introduction

Locations are not to be confused with sales channels. Locations are not the equivalent of sales processes.

A location represents a physical place which has its own independent name, address and opening hours. A sales channel may
represent the process of selling items for single location or multiple locations.