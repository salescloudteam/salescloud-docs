# SalesCloud Payment Request API

## Introduction

The Payment Request API is an isolated restful api with the sole purpose of processing payment requests that can automaticallly scale to 1 000 cpus
and 2 000 gb ram.

The service is completely atomic and acid compliant.

## Service Base URL

The service lives at: https://service-payment-request.salescloud.is

## Required URL Parameters

The service requires a 3 parameters in the url like so:

The service lives at: https://service-payment-request.salescloud.is/v/{version}/{organization}/{channel}/{payment_instance}

Version
> The {version} part is the version identifier. For now there is only one: 1.

Organization 
> The {organization} part is the uuid of the organization that owns the payment.

Channel
> The {channel} part is the uuid of the sales channel that belongs to the organization.

Payment Instance
> The payment instance part is the uuid of the payment method instance that belongs to the organization.

If any of these parameters are wrong then the payment service will reply with a 404 not found.

## Responses

The payment service api will only serve with 2 distinct http codes and will always only accept valid json and return json.

200
> The api wil respond with a http response code 200 if the payment was actually requested. This does not mean that the
> payment was accepted.

404
> The api will respond with a http response code 404 if payment was NOT requested. This means that something was wrong with the request.

Body

> The body of the return contains top level properties that indicate if the payment was approved or declined.

Content-Type

> The content-type of the response is always valid json.

### Declined Response 
``` json
{
    "success": false,
    "errorCode": 3,
    "paymentTransaction": {
        "namespace": "valitor_greidslugatt",
        "amount": 100,
        "currency_code": "ISK",
        "status": "failure",
        "message": "The card was declined.",
        "payload": {
            // this object will contain payment method specific data
        }
    }
}
```

### Approved Response 
``` json
{
    "success": true,
    "errorCode": 0,
    "paymentTransaction": {
        "namespace": "valitor_greidslugatt",
        "amount": 100,
        "currency_code": "ISK",
        "status": "success",
        "message": "The payment was approved.",
        "payload": {
            // this object will contain payment method specific data
        }
    }
}
```

## Requesting Payments

The payment service api only accepts valid json in the body of post request and requires no other formal authentication.

### Required Properties

namespace

> String. The namespace of the payment method. It must match the namespace of the payment instance uuid in the url.

amount

> Integer. The amount of the payment.

currency_code

> String. The currency code of the payment.

details

> Object. The values to the UI fields required by the payment method.

### Optional Properties

requestId

> String. Your external reference of the payment.

billingInfo

> Object. The billing info of the payment.

shippingInfo

> Object. The shipping info of the payment.

comment

> String. A comment for the order.

### Simple Example

``` json
{
  "namespace": "valitor_greidslugatt",
  "requestId": "123456",
  "amount": 1000,
  "currency_code": "ISK",
  "details": {
    "cardNumber": "123456789",
    "cardSecurityCode": "123",
    "expiryMonth": "12",
    "expiryYear": "2027"
  }
}
```

### Advanced Example

``` json
{
  "namespace": "valitor_greidslugatt",
  "requestId": "123456",
  "amount": 1000,
  "currency_code": "ISK",
  "details": {
    "cardNumber": "123456789",
    "cardSecurityCode": "123",
    "expiryMonth": "12",
    "expiryYear": "2027"
  },
  "billingInfo": {
    "name": "John Doe",
    "phone": "123456789",
    "email" : "hello@world.is"
    "country": "IS"
  },
  "shippingInfo": {
    "postal_code": "123",
    "address": "Þetta er bara test",
    "country": "IS"
  },
  "deliveryService": {
    "uuid": "e25abb40-6ac5-11ea-b6ac-098bba9d8176",
    "title": "Home Delivery"
  },
  "deliveryServicePrice": {
    "amount": 10,
    "currency_code": "ISK"
  },
  "coupon": null, // The coupon uuid
  "comment": "This is a comment for the order.",
  "location": null, // The uuid of the location
  "deliveryTime": null, // Unix timestamp in seconds
  "items": [
    {
      "uuid": "18f2d14e-2d1d-4e5f-9a3c-e174a92f6ca1", // required
      "quantity": 1, // required
      "discountPercent": 0, // optional
      "startsAtTimeInMillis": 0, // required for event items
      "endsAtTimeInMillis": 0, // optional for event items
      "variations": [ // optional
        {
          "uuid": "6479707c-31b3-47c2-b39f-d7084466daf4" // required
        },
        {
          "uuid": "4bba38e6-d3de-4959-b2c0-7b94331ec759" // required
        }
      ]
    }
  ],
  "unit": null // The uuid of an individual unit,
  "discounts": [] // Array of applied discounts
}
```
