# Customers

## Created

This event is emitted after a new customer has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('newCustomerCreated', function(customer) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing customer has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('customerUpdated', function(customer) {
    // Add your logic here.
})

```

## Disabled

This event is emitted after an existing customer has been disabled.

An app can listen to the event as shown below.

``` js

context.eventbus.on('customerDisabled', function(customer) {
    // Add your logic here.
})

```