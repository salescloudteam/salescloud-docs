# JS

## Introduction
Gives your app the capability to define javascript.

## Base definition

``` js
const exampleJS = {
    title: 'Facebook Pixel',
    description: 'Inserts JS for facebook pixel',
    namespace: 'facebook_pixel',
    callbacks: {
    }
}
```

## Example

``` js
const exampleJS = {
    title: 'Facebook Pixel',
    description: 'Inserts JS for facebook pixel',
    namespace: 'facebook_pixel',
    callbacks: {
        renderJS(context) {

            let facebookPixelCode = '!function(f,b,e,v,n,t,s)\n' +
'  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n' +
'  n.callMethod.apply(n,arguments):n.queue.push(arguments)};\n' +
'  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';\n' +
'  n.queue=[];t=b.createElement(e);t.async=!0;\n' +
'  t.src=v;s=b.getElementsByTagName(e)[0];\n' +
'  s.parentNode.insertBefore(t,s)}(window, document,\'script\',\n' +
'  \'https://connect.facebook.net/en_US/fbevents.js\');\n' +
'  fbq(\'init\', \'' + context.settings.pixel_code + '\');\n' +
'  fbq(\'track\', \'PageView\');\n'

            return facebookPixelCode
        }
    }
}
```

## Registration

``` js
module.exports = {
    app : app,
    js: [
        exampleJS
    ]
}
```