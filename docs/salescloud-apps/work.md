# Work

## Created

This event is emitted after new work has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('workCreated', function(work) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing customer has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('workUpdated', function(work) {
    // Add your logic here.
})

```