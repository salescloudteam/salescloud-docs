# Delivery Method Instances

## Created

This event is emitted after a new delivery method instance has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('deliveryMethodInstanceCreated', function(deliveryInstance) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing delivery method instance has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('deliveryMethodInstanceUpdated', function(deliveryMethodInstance) {
    // Add your logic here.
})

```