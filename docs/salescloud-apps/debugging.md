# Debugging

Use the onRun callback triggered by the "Run" operation in the developer ide for debugging purposes.

Use the errors section of your application to track & follow errors caused by running your app.

Applications that cause too many errors risk being removed from the appstore.