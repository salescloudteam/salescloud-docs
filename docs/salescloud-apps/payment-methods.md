# Payment Methods

## Introduction

A payment method controls how a payment is handled from its creation to completion. Each defined payment method has a unique namespace.

Think of cash as a distinct payment method and a payment there of as the simplest. However nowadays payment methods are much more complex.
Think of a card payment as another payment method with processing requirements which are completely different.

Payment methods however complex they seem have similarities in how they are handled. For example both cash & card payments 
must preferably support refunds and cancellations. Accepting payments is just one side of the same sword.

## Base definition

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
    }
}
```

## Callbacks

### New Transaction

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
        newTransaction: async function(paymentTransaction, paymentInstance) {

            // Do custom changes here

            return paymentTransaction
        }
    }
}
```

### Charge Transaction

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
        chargeTransaction: async function(paymentTransaction, paymentInstance, details) {

            // Do your payment processing here

            return paymentTransaction
        }
    }
}
```

### Cancel Transaction

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
        cancelTransaction: async function(paymentTransaction, paymentInstance, details) {

            // Do your payment processing here

            return paymentTransaction
        }
    }
}
```

### Credit Transaction

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
        creditTransaction: async function(paymentTransaction, paymentInstance, details) {

            // Do your payment processing here

            return paymentTransaction
        }
    }
}
```

### Resolve Transaction

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
        resolveTransaction: async function(paymentTransaction, paymentInstance) {

            // Do your payment processing here

            return paymentTransaction
        }
    }
}
```

### Complete Example

``` js
const paymentMethod = {
    title: "My Custom Payment Method",
    description: "Log custom payments of Custom Payment Method",
    namespace: "my_custom_payment_method",
    configFields: [],
    uiFields: [],
    callbacks: {
        newTransaction: async function(paymentTransaction, paymentInstance) {
        
                    // Do custom changes here
        
                    return paymentTransaction
        },
        chargeTransaction: async function(paymentTransaction, paymentInstance, details) {

            // Do your payment processing here

            return paymentTransaction
        },
        cancelTransaction: async function(paymentTransaction, paymentInstance, details) {

            // Do your payment processing here

            return paymentTransaction
        },
        creditTransaction: async function(paymentTransaction, paymentInstance, details) {

            // Do your payment processing here

            return paymentTransaction
        },
        resolveTransaction: async function(paymentTransaction, paymentInstance) {

            // Do your payment processing here

            return paymentTransaction
        }
    }
}
```