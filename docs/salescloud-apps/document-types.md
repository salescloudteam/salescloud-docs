# Document Types

Document types of orders such as receipts and kitchen receipts. A document type receives an order and returns an html document based on the order.

As a developer this allows you to create custom types of documents for orders. 

This definition extends to the SalesCloud POS and makes use of all harwdware such as printers.

## Example

``` js

const documentType = {
    namespace: 'my_custom_ticket',
    title: 'Ticket',
    description: 'Prints out a ticket',
    callbacks: {
        isAvailable: function(order) {

            if(!order.commerce_line_items) {
                return false
            }

            if(order.commerce_line_items.length == 0) {
                return false
            }

            return true
        },
        renderDocument: function(order) {
            return '<html><body>This is my ticket</body></html>'
        }
    }
}
```