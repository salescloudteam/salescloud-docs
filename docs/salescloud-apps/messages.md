# Messages

## Created

This event is emitted after a new message has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('messageCreated', function(message) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing message has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('messageUpdated', function(message) {
    // Add your logic here.
})

```