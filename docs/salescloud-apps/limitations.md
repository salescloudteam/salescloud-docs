# Limitations

Your application may not consume more than:
 * 128 mb of ram
 * 90 seconds of execution time (after the onStop lifecycle callback).
 * 60 seconds of blocking execution time in general.
 
Causing a never ending loop will lead to the platform forcefully terminating the execution of application.