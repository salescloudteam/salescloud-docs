# Developing

## IDE & Writing Code

It is only possible to add new code by using the provided IDE in the developer portal. 

Remember to save your code via the "Save" operation in the ide toolbar as you type
as there is no autosave.

Saving your code will perform a syntax check and alert you to any errors in your code.

Please make use of the "Run" operation in the ide toolbar to test your code as you develop. Refer to the onRun callback in the lifecycle callbacks to
learn more about using the "Run" operation.

## Style

There is no particular code style that you are required to follow. Follow your own best practices.

## Files & Folders

The IDE in the developer portal only allows you to define your code in a single file. There is no concept of folders & files as of yet. Split your code into functions to maintain good quality code.

## Runtime / Language

Your code will run in a nodejs virtual machine with its own process isolated from the main platform. Variables that you define in your application is not shared or exposed to other applications.

You must write code that adherese to and follows nodejs best practices.
