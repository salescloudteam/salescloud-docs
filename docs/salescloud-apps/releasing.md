# Releasing

You must create a release in order to publish your application to the appstore.

Releasing an application creates a new version of your application. Version numbers start 1 and increment with each release.

The SalesCloud Platform keeps a copy of each versions code.

When users install your application to their organization then they install the most recently released version of your application.

Creating new releases does not automatically update or upgrade current installations of your application.

Users must manually update your application through the appstore. In the future we will provide automatic updating similar to ios and android appstores.

Currently it is possible to revert versions, you can only revert a mistake by releasing a new version with a fix.