# Checkout Sections

## Introduction
A checkout section represents collection of fields represented on a specific step in a checkout.

Please do not confuse checkout sections with checkout steps.

## Base Definition

``` js
const checkoutSection = {
    title: "My Custom Checkout Section",
    description: "We need to know something to process your order",
    namespace: "my_custom_checkout_section",
    configFields: [],
    uiFields: [],
    callbacks: {
    }
}
```

## Registration

``` js
module.exports = {
    app : app,
    checkoutSections: [
        checkoutSection
    ]
}
```