# Routes

Routes give you the ability to add navigational elements and new pages to existing platforms.

As a developer you may want to add new custom pages to the platform.


## Base definition

``` js
const myCustomRoute = {
	title: 'My Custom page',
	icon: 'earth',
	path: 'my-custom-page-path',
	platform: 'admin',
	weight: 0,
	type: 'nativeNormal'
}
```

## Registration

``` js
module.exports = {
    app : app,
    routes: [
        myCustomRoute
    ]
}
```