# Orders

## Created

This event is emitted after a new order has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('orderCreated', function(order) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing message has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('orderUpdated', function(order) {
    // Add your logic here.
})

```