# Testing

Writing your code in a remote platform can be daunting. For starter, you may know little about the platform that is actually 
running your code and you may want to test individual functions in your code before making a release.

Please make use of the onRun lifecycle callback to test your code in the ide of the developer portal.

When you initiate a "Run" operation the platform will do the following:
* Load the code that you have most recently saved.
* Spin up a nodejs virtual machine
* Compile your code in the virtual machine
* Execute the onRun callback
* Grab any console.log information and display it to you in the IDE.

This is the preferred way to test your application.