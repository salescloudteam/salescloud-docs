# Invoices

## Created

This event is emitted after a new invoice has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('invoiceCreated', function(invoice) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing invoice has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('invoiceUpdated', function(invoice) {
    // Add your logic here.
})

```