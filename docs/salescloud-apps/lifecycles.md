# Lifecycles

## Introduction

The platform will call certain lyfecycle callbacks when registering and unregistering your application to help you prepare your application for running your code.

## Callbacks

The callbacks are declared as shown below.

``` js
const app = {
    lifeCycle: {
        onInstall: function(context, uiValues) {
            console.log('Installing')

            // This callback runs when your application is being installed.
            // Do your validation/setup here and indicate if the installation should succeed or fail.

            return {
                success: true,
                message: 'Ready for use'
            }
        },
        onUninstall: function(context) {
            console.log('Uninstalling')

            // This callback runs when your application is being uninstalled.
            // Do your cleanup operation here and optionally decline the uninstall.

            return {
                success: true,
                message: 'Successfully uninstalled'
            }
        },
        onRunRequest: async function(context, req) {
            // This callback runs when your is intentionally/manually run externally.
            // Use this callback to respond to custom external requests.
        },
        onRun: async function(context) {
            // This callback runs when your is intentionally/manually run from the ide.
            // Use this callback to test your code.
        },
        onStart: function(context) {
            console.log('Starting')
            // This callback signals that the platform has registered your app for running. Not all applications have been registered at this time.
            // Use this callback for preparing your app. Registering globals, subscribing to platform & custom events.
        },
        onResume: function(context) {
            console.log('Resuming')
            // This callback signals that the plaftorm has started your application and all other applications successfully.
            // Use this callback for signaling other apps by emitting platform & custom events.
        },
        onPause: function(context) {
            console.log('Pausing')
            // This callback signals that the platform has already executed the required request/operation and is preparing to release your application and all others.
            // Use this callback for signaling other apps by emitting platform & custom events.
        },
        onStop: function(context) {
            console.log('Stopping')
            // This callback signals that the platform will release your app after calling this function and may have already released other apps.
            // Use this callback to do cleanup such as unsubscribing to events, releasing memory, etc.
        }
    },
}
```

### onInstall

This callback runs when your application is being installed.
Do your validation/setup here and indicate if the installation should succeed or fail.

As a developer this callback will help you and your users to get your application working right off the bat.

#### Declaration

``` js
{
    ...
    onInstall: function(context, uiValues) {
        console.log('Installing')

        // This callback runs when your application is being installed.
        // Do your validation/setup here and indicate if the installation should succeed or fail.

        return {
            success: true,
            message: 'Ready for use'
        }
    }
}
```

### onUninstall

This callback runs when your application is being uninstalled. Do your cleanup operation here and optionally decline the uninstall.

As a developer this callback will help you and your users to get your application uninstalled properly & gracefully.

#### Declaration

``` js
{
    ...
    onUninstall: function(context) {
        console.log('Uninstalling')

        // This callback runs when your application is being uninstalled.
        // Do your cleanup operation here and optionally decline the uninstall.

        return {
            success: true,
            message: 'Successfully uninstalled'
        }
    }
}
```

### onRun

This callback runs when your is intentionally/manually run from the ide. Use this callback to test your code.

#### Declaration

``` js
{
    ...
    onRun: async function(context) {
        // This callback runs when your is intentionally/manually run from the ide.
        // Use this callback to test your code.
    },
}
```

### onRunRequest

This callback runs when your is intentionally/manually run externally. Use this callback to respond to custom external requests.

As Developer this callback will help you with to forcefully run your application. Like a webhook to start and run your application.

#### Declaration

``` js
{
    ...
    onRun: async function(context) {
        // This callback runs when your is intentionally/manually run from the ide.
        // Use this callback to test your code.
    },
}
```

#### How to run this callback externally

This requires you to know 3 things:
* The uuid of your app referred to as {uuid}.
* The uuid of the organization that has installed your app referred to as {organization}.
* The uuid of the installation of your app referred to as {installation_uuid}

Base url to execute the app is as follows:
> https://graph.salescloud.is/runApp/{uuid}

It accepts GET/PUT/POST/DELETE and it is up to your code in the callback to respond to these accordingly.

The endpoint above requires two headers as follows:

> X-SALESCLOUD-APP-INSTALL-ID

This is the {installation_uuid}.

> X-SALESCLOUD-ORGANIZATION-ID

This is the {organization_uuid}
