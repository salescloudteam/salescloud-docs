# Dash

## Introduction

A dash represents a dynamically rendered card for presentation purposes such as reports and dashboards.

## Base Definition

Just enough to register your dash. 

uiFields are optional fields that users can use to modify the result of the rendered dash. Like a select list of months.
Changing a uiField will trigger a rerender. They represent a frequently used settings.

configFields are optional fields to configure your dash and represent a setting that is not frequently changed.

``` js
const dashType = {
    title: "My Custom Dash Type",
    description: "This is dash shows a summary of payments",
    namespace: "my_custom_dash_type",
    configFields: [],
    uiFields: [],
    callbacks: {
    }
}
```

## Example of a table

An example of how to generate a table report.

``` js
const dashType = {
    title: "My Custom Dash Type",
    description: "This is dash shows a summary of payments",
    namespace: "my_custom_dash_type",
    configFields: [],
    uiFields: [],
    callbacks: {
        render: async function(dashInstance, uiValues) {
            
            const rows = []

            return {
                type: 'table',
                content: {
                    caption: 'Payments',
                    'no-data-text': 'No payments found',
                    'hide-default-footer': true,
                    headers: [
                        {
                            text:  'Payment method',
                            value: 'payment_method'
                        },
                        {
                            text:  'Currency',
                            value: 'currency'
                        },
                        {
                            text:  'Count',
                            value: 'count'
                        },
                        {
                            text:  'Amount',
                            value: 'amount',
                            align: 'right'
                        }
                    ],
                    rows: rows
                }
            }
        }
    }
}
```

### Example of a Google Chart

An example of a chart showing a summary of payments over time.

``` js

const dashType = {
    namespace: 'payments_over_time',
    title: 'Payments over time',
    description: 'A line chart of payments over time',
       callbacks: {
        render: async function(dashInstance, uiValues) {

            if(appContext === null) {
                return 'Unable to run query'
            }

            const granularity = dashInstance.data.granularity || 'days'

            const now = new Date()

            let year = now.getFullYear()
            let month = now.getMonth()

            if(uiValues && uiValues.month_and_year) {
                const monthYearSelected = uiValues.month_and_year.split('_')
                year = monthYearSelected[0]
                month = monthYearSelected[1]
            }

            const firstDay = new Date(year, month, 1)
            const lastDay = new Date(year, month, 31, 23, 59, 59)

            return getPaymentStats(firstDay.getTime() / 1000, lastDay.getTime() / 1000, null).then(paymentStats => {

                let chartData = [
                    ['Hours', 'Revenue']
                ]

                Object.keys(paymentStats).forEach(currency => {
                    let currencyStats = paymentStats[currency]

                    Object.keys(currencyStats[granularity]).forEach(entry => {
                        let entryStats = currencyStats[granularity][entry]
                        chartData.push([entry, entryStats.totalAmount])
                    })

	            })

                return {
                    type: 'gchart',
                    content: {
                        chartType: 'line',
                        chartData: chartData
                    }
                }
            })
        },
        alterUIFields: async function(uiFields) {

            const now = new Date()

            let current = new Date('2015', 1)

            const dateOptions = []

            while(current.getTime() < now.getTime()) {
                const month = current.toLocaleString('default', { month: 'long' })
                dateOptions.push({
                    key: current.getFullYear() + '_' + current.getMonth(),
                    label: month + ' ' + current.getFullYear()
                })

                current = current.addMonths(1)
            }

            uiFields[0].options = dateOptions.reverse()

            //uiFields[0]
            
            return uiFields
        }
    },
    uiFields: [
        {
            key: 'month_and_year',
            type: 'select',
            label: 'Month',
            options: [],
            cols: 6
        },
        {
            key: 'channel',
            type: 'select',
            label: 'Channel',
            options: [],
            cols: 6
        }
    ],
    configFields: [
        {
            key: 'granilurity',
            type: 'select',
            label: 'Granularity',
            options: [
                {
                    key: 'hours',
                    label: 'Hours'
                },
                {
                    key: 'days',
                    label: 'Days'
                },
                {
                    key: 'weeks',
                    label: 'Weeks'
                }
            ]
        }
    ]
}
```

## Registration

``` js
module.exports = {
    app : app,
    dashTypes: [
        dashType
    ]
}
```