# Subscriptions

## Created

This event is emitted after a new subscription has been created/inserted.

An app can listen to the event as shown below.

``` js

context.eventbus.on('newSubscriptionCreated', function(subscription) {
    // Add your logic here.
})

```

## Cancelled

This event is emitted after an existiing subscription has been cancelled.

An app can listen to the event as shown below.

``` js

context.eventbus.on('subscriptionCancelled', function(subscription) {
    // Add your logic here.
})

```

## Ended

This event is emitted after an existing subscription has ended.

An app can listen to the event as shown below.

``` js

context.eventbus.on('subscriptionCancelled', function(subscription) {
    // Add your logic here.
})

```

## Updated

This event is emitted after an existing subscription has been updated.

An app can listen to the event as shown below.

``` js

context.eventbus.on('subscriptionUpdated', function(subscription) {
    // Add your logic here.
})

```

## Renewed

This event is emitted after an existing subscription has been renewed.

An app can listen to the event as shown below.

``` js

context.eventbus.on('subscriptionRenewed', function(subscription) {
    // Add your logic here.
})

```