# CSS

## Introduction
Gives your app the capability to define css.

## Base definition

``` js
const exampleCSS = {
    title: 'Example XSS',
    description: 'Inserts CSS',
    namespace: 'example_css',
    callbacks: {
    }
}
```

## Example

``` js
const exampleCSS = {
    title: 'Example XSS',
    description: 'Inserts CSS',
    namespace: 'example_css',
    callbacks: {
        renderJS(context) {
            return 'body { background: #000; }'
        }
    }
}
```

## Registration

``` js
module.exports = {
    app : app,
    css: [
        exampleCSS
    ]
}
```